using Nikfort.RealtyBot.Console;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InlineKeyboardButtons;
using Telegram.Bot.Types.ReplyMarkups;

namespace Nikfort.RealtyBot.ConsoleApplication
{
    public static class KeyboardLayout
    {
        private static readonly IUserFilterManager FilterManager = MockUserFilterManager.Instance;

        public static ReplyKeyboardMarkup Main => new ReplyKeyboardMarkup
        {
            Keyboard = new[]
            {
                new KeyboardButton[]
                {
                    "�����"
                }
            },
            ResizeKeyboard = true
        };

        public static InlineKeyboardMarkup RealtyObjectActions()
        {
            return new InlineKeyboardMarkup
            {
                InlineKeyboard = new InlineKeyboardButton[][]
                {
                    new[]
                    {
                        new InlineKeyboardCallbackButton("�������������", "1"),
                        new InlineKeyboardCallbackButton("�����������", "2"),
                        new InlineKeyboardCallbackButton("�������", "3"),
                    }
                }
            };
        }

        public static InlineKeyboardMarkup SearchFilters(long chatId)
        {
            var fiters = FilterManager.GetUserFilter(chatId);
            return new InlineKeyboardMarkup
            {
                InlineKeyboard = new InlineKeyboardButton[][]
                {
                    new[]
                    {
                        new InlineKeyboardCallbackButton(fiters.Amount == "1-2" ? "\u2714 1-2 ���" : "1-2 ���", "Amount:1-2"),
                        new InlineKeyboardCallbackButton(fiters.Amount == "2-3" ? "\u2714 2-3 ���" : "2-3 ���", "Amount:2-3"),
                        new InlineKeyboardCallbackButton(fiters.Amount == "3-4" ? "\u2714 3-4 ���" : "3-4 ���", "Amount:3-4"),
                    },
                    new[]
                    {
                        new InlineKeyboardCallbackButton(fiters.Area == "<40" ? "\u2714 �� 40 ��.�." : "�� 40 ��.�.", "Area:<40"),
                        new InlineKeyboardCallbackButton(fiters.Area == "40-60" ? "\u2714 40-60 ��.�." : "40-60 ��.�.", "Area:40-60"),
                        new InlineKeyboardCallbackButton(fiters.Area == ">60" ? "\u2714 �� 60 ��.�." : "�� 60 ��.�.", "Area:>60"),
                    },
                    new[]
                    {
                        new InlineKeyboardCallbackButton(fiters.RoomQuantity == "1" ? "\u2714 1 ����." : "1 ����.", "RoomQuantity:1"),
                        new InlineKeyboardCallbackButton(fiters.RoomQuantity == "2" ? "\u2714 2 ����." : "2 ����.", "RoomQuantity:2"),
                        new InlineKeyboardCallbackButton(fiters.RoomQuantity == "3" ? "\u2714 3 ����." : "3 ����.", "RoomQuantity:3"),
                    }, 
                    new []
                    {
                        new InlineKeyboardCallbackButton("�����", "Action:Find"),
                    }
                }
            };
        }
    }
}