﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Nikfort.RealtyBot.Console;
using Nikfort.RealtyBot.ConsoleApplication;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
#pragma warning disable 4014

namespace Nikfort.RealtyBot.ConsoleApplication
{
    class Bot
    {
        private static readonly ConcurrentDictionary<long, int> LasMessagetIdByChatId = new ConcurrentDictionary<long, int>();

        private readonly TelegramBotClient _bot;
        private readonly IUserFilterManager _filterManager = MockUserFilterManager.Instance;
        private readonly IRealtyObjectService _realtyObjectService = new MockRealtyObjectService();

        public Bot()
        {
            _bot = new TelegramBotClient(System.Configuration.ConfigurationManager.AppSettings["token"]);
            _bot.OnCallbackQuery += async (sender, args) =>
            {
                var chatId = args.CallbackQuery.Message.Chat.Id;
                try
                {
                    UserFilter filters = _filterManager.GetUserFilter(chatId);
                    var filterKey = args.CallbackQuery.Data.Split(':')[0];
                    var filterValue = args.CallbackQuery.Data.Split(':')[1];
                    if (filterKey == "Amount") filters.Amount = filterValue;
                    if (filterKey == "Area") filters.Area = filterValue;
                    if (filterKey == "RoomQuantity") filters.RoomQuantity = filterValue;
                    _filterManager.SetUserFilter(chatId, filters);

                    _bot.AnswerCallbackQueryAsync(args.CallbackQuery.Id);
                    if (filterKey == "Action")
                    {
                        await _bot.SendTextMessageAsync(chatId, "Результаты поиска:");
                        var realtyObjectLinks = _realtyObjectService.GetRealtyObjectLinks(filters);
                        foreach (var link in realtyObjectLinks)
                        {
                            await _bot.SendTextMessageAsync(chatId, $"<a href='{link}'>Ссылка</a>", ParseMode.Html, replyMarkup: KeyboardLayout.RealtyObjectActions());
                        }
                    }
                    if (LasMessagetIdByChatId.ContainsKey(chatId) && filterKey != "Action")
                    {
                        await _bot.EditMessageReplyMarkupAsync(chatId, LasMessagetIdByChatId[chatId], replyMarkup: KeyboardLayout.SearchFilters(chatId));
                    }
                    else
                    {
                        var message = await _bot.SendTextMessageAsync(chatId, "Фильтры поиска:", replyMarkup: KeyboardLayout.SearchFilters(chatId));
                        LasMessagetIdByChatId[chatId] = message.MessageId;
                    }
                }
                catch (Exception ex)
                {
                    await _bot.SendTextMessageAsync(chatId, "Произошла неизвестная ошибка!");
                    _bot.SendTextMessageAsync(chatId, "Фильтры поиска:", replyMarkup: KeyboardLayout.SearchFilters(chatId));
                }

            };
            _bot.OnMessage += async (sender, args) =>
            {
                var receivedMessage = args.Message;
                if (receivedMessage == null || receivedMessage.Type != MessageType.TextMessage) return;
                var sentMessage = await _bot.SendTextMessageAsync(receivedMessage.Chat.Id, "Выберите фильтры:", replyMarkup: KeyboardLayout.SearchFilters(receivedMessage.Chat.Id));
                LasMessagetIdByChatId[sentMessage.Chat.Id] = sentMessage.MessageId;
            };
        }

        public void Start()
        {
            _bot.StartReceiving();
        }

        public void Stop()
        {
            _bot.StopReceiving();
        }
    }
}
