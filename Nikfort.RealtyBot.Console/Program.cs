﻿using System;
using System.Linq;
using Telegram.Bot.Types.Enums;

namespace Nikfort.RealtyBot.ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var bot = new Bot();
            bot.Start();
            System.Console.WriteLine("Press any key to exit");
            System.Console.ReadLine();
            bot.Stop();
        }
    }
}
