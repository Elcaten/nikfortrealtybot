﻿using System.Collections.Generic;
using Nikfort.RealtyBot.Console;

namespace Nikfort.RealtyBot.ConsoleApplication
{
    class MockRealtyObjectService : IRealtyObjectService
    {
        public IEnumerable<string> GetRealtyObjectLinks(UserFilter filter)
        {
            return new[]
            {
                "http://operator-walter-71874.bitballoon.com/",
                "http://farmer-analyses-37174.bitballoon.com/"
            };
        }
    }
}