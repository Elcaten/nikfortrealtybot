﻿using System.Collections.Generic;
using Nikfort.RealtyBot.Console;

namespace Nikfort.RealtyBot.ConsoleApplication
{
    public interface IRealtyObjectService
    {
        IEnumerable<string> GetRealtyObjectLinks(UserFilter filter);
    }
}