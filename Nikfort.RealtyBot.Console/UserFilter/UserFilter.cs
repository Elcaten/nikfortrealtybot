﻿namespace Nikfort.RealtyBot.Console
{
    public class UserFilter
    {
        public string Amount { get; set; }
        public string Area { get; set; }
        public string RoomQuantity { get; set; }
    }
}