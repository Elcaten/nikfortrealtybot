﻿namespace Nikfort.RealtyBot.Console
{
    public interface IUserFilterManager
    {
        UserFilter GetUserFilter(long chatId);
        void SetUserFilter(long chatId, UserFilter userFilter);
    }
}