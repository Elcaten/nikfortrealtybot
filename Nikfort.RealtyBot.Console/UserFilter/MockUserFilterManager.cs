﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Nikfort.RealtyBot.Console
{
    public class MockUserFilterManager : IUserFilterManager
    {
        private static readonly string FiltersFilePath = "D:\\filters.json";
        private static IUserFilterManager _instance;
        private readonly IDictionary<long, UserFilter> _userFilters;

        public static IUserFilterManager Instance => _instance ?? (_instance = new MockUserFilterManager());

        private MockUserFilterManager()
        {
            if (!File.Exists(FiltersFilePath)) File.Create(FiltersFilePath).Close();
            var json = File.ReadAllText(FiltersFilePath);
            _userFilters = JsonConvert.DeserializeObject<IDictionary<long, UserFilter>>(json) ?? new Dictionary<long, UserFilter>();
        }

        public UserFilter GetUserFilter(long chatId)
        {
            if (!_userFilters.ContainsKey(chatId)) _userFilters[chatId] = new UserFilter();
            return _userFilters[chatId];
        }

        public void SetUserFilter(long chatId, UserFilter userFilter)
        {
            _userFilters[chatId] = userFilter;
        }

        ~MockUserFilterManager()
        {
            var content = JsonConvert.SerializeObject(_userFilters);
            File.WriteAllText(FiltersFilePath, content);
        }
    }
}